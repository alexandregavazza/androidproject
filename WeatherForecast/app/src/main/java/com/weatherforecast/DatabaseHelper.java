package com.weatherforecast;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "location.db";

    private static final String TABLE_NAME = "location_table";
    private static final String Col_1 = "ID";
    private static final String Col_2 = "name_loc";
    private static final String Col_3 = "lat_loc";
    private static final String Col_4 = "long_loc";
    private static final String Col_5 = "Country_loc";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+"( ID INTEGER PRIMARY KEY, NAME_LOC TEXT, LAT_LOC TEXT, LONG_LOC TEXT,COUNTRY_LOC TEXT )");
        Log.d("MyDB", "onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        Log.d("MyDb","onUpgrade");
        onCreate(db);
    }

    public boolean insertData(Integer cityID,  String name_loc, Double lat_loc, Double long_loc, String country_loc){
        SQLiteDatabase db =  this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Col_1, cityID);
        contentValues.put(Col_2, name_loc);
        contentValues.put(Col_3, lat_loc);
        contentValues.put(Col_4, long_loc);
        contentValues.put(Col_5, country_loc);

        long result =  db.insert(TABLE_NAME,null,contentValues);

        if (result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData(){
        SQLiteDatabase db =  this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }

    public Integer deleteData(String id){
        SQLiteDatabase db =  this.getWritableDatabase();

        Log.d("REMOVING...", id);
        return db.delete(TABLE_NAME, "ID = ?",new String[]{id});
    }
}