package com.weatherforecast;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ChooseCity extends AppCompatActivity {
    private static final String URL_CITY_LIST_API = "http://geodb-free-service.wirefreethought.com/v1/geo/cities?limit=5&offset=0&namePrefix=%s";

    DatabaseHelper myDB;
    private Button _btnAddNewCity;
    private EditText _txtNewLocation;
    private RadioGroup rbg;
    private RadioButton rb;

    private LinearLayout ll;
    private LinearLayout l2;

    private ArrayList<City> SQLLocations = new ArrayList<>();
    private ArrayList<City> cityList = new ArrayList<>();
    private int _gpsLocationCityId = -1;

    public class City
    {
        private int _cityId;
        private String _cityName;
        private Double _lat;
        private Double _lng;
        private String _countryCode;

        public City(int cityId, String cityName)
        {
            _cityId = cityId;
            _cityName = cityName;
        }

        public City(int cityId, String cityName, Double lat, Double lng, String countryCode)
        {
            _cityId = cityId;
            _cityName = cityName;
            _lat = lat;
            _lng = lng;
            _countryCode = countryCode;
        }

        public int CityId()
        {
            return _cityId;
        }

        public String CityName()
        {
            return _cityName;
        }

        public Double Lat()
        {
            return _lat;
        }

        public Double Lng()
        {
            return _lng;
        }

        public String CountryCode()
        {
            return _countryCode;
        }

        public void CityId(int cityId) { _cityId = cityId; }

        public void CityName(String cityName)
        {
            _cityName = cityName;
        }

        public void Lat(Double lat)
        {
            _lat = lat;
        }

        public void Lng(Double lng)
        {
            _lng = lng;
        }

        public void CountryCode(String countryCode)
        {
            _countryCode = countryCode;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_city);

        myDB = new DatabaseHelper(this);

        Button btnDeleteCity = (Button) findViewById(R.id.btnDeleteCity);
        Button btnBackMain = (Button) findViewById(R.id.btnBackMain);
        _btnAddNewCity = (Button) findViewById(R.id.btnAddNewCity);
        Button _btnSearchCity = (Button) findViewById(R.id.btnSearchCity);
        Button _btnViewWeatherCity = (Button) findViewById(R.id.btnViewWeatherCity);
        _txtNewLocation = (EditText)findViewById(R.id.txtNewLoacation);
        ll = findViewById(R.id.Lresult);
        l2 = findViewById(R.id.l2);
        rbg = new RadioGroup(this);
        rbg.setBackgroundResource(R.color.colorWeather);

        RadioButton rbCelsius = (RadioButton) findViewById(R.id.rbCelsius);
        RadioButton rbFah = (RadioButton) findViewById(R.id.rbFahrenheit);

        //Clear List
        ll.removeAllViews();
        l2.removeAllViews();

        AddLocation();
        FillLocationsRecord();

        // *** DELETE CITY BUTTON *** //
        btnDeleteCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = ll.getChildCount();
                boolean checked = false;

                Log.d("ChooseCity/DELETE CITY", "Count: " + count);
                for(int i = 0; i < count; i++) {
                    CheckBox chkCitySelected = (CheckBox) ll.getChildAt(i);
                    String cityId = String.valueOf(chkCitySelected.getId());

                    Log.d("ChooseCity/DELETE CITY", (String) "ID: " + cityId + " / Label: " + chkCitySelected.getText());
                    Log.d("ChooseCity/DELETE CITY", String.valueOf(chkCitySelected.isChecked()));

                    if (chkCitySelected.isChecked()) {
                        checked = true;
                        Integer isDeleted = myDB.deleteData(cityId);
                        Toast.makeText(ChooseCity.this, "City deleted!", Toast.LENGTH_LONG).show();
                        Log.d("ChooseCity/DELETE CITY", "DATA DELETED: " + isDeleted);
                    }
                }

                FillLocationsRecord();

                if (!checked)
                    Toast.makeText(ChooseCity.this, "Choose at least one city", Toast.LENGTH_LONG).show();
            }
        });

        // *** VIEW WEATHER CITY BUTTON *** //
        _btnViewWeatherCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = ll.getChildCount();
                int qtyCheckd = 0;
                int lastIdChecked = -1;

                Log.d("ChooseCity/VIEW WEATHER", (String) "GPS CITY LOCATION? " + _gpsLocationCityId);
                City city = null;

                if (_gpsLocationCityId == -1) {
                    for (int i = 0; i < count; i++) {
                        CheckBox chkCitySelected = (CheckBox) ll.getChildAt(i);

                        Log.d("ChooseCity/VIEW WEATHER", (String) "ID: " + chkCitySelected.getId() + " / Label: " + chkCitySelected.getText() + " / " + String.valueOf(chkCitySelected.isChecked()));

                        if (chkCitySelected.isChecked()) {
                            qtyCheckd++;
                            lastIdChecked = chkCitySelected.getId();
                        }
                    }

                    city = GetCityInArray(lastIdChecked, SQLLocations);
                    if (city != null)
                        Log.d("ChooseCity/VIEW WEATHER", city.CityName() + " / " + city.CityId() + " / " + city.Lat() + " / " + city.Lng());
                    else
                        Log.d("ChooseCity/VIEW WEATHER", "City is NULL VALUE");
                }
                else
                {
                    Log.d("ChooseCity/VIEW WEATHER", "GPS LOCATION SELECTED");
                    city = new City(0, "", 0.0, 0.0, "");
                }

                if (qtyCheckd == 1 || _gpsLocationCityId == 0)
                {
                    Toast.makeText(ChooseCity.this, "CITY SELECTED!", Toast.LENGTH_SHORT).show();
                    String metric = "metric";

                    if (rbCelsius.isChecked())
                    {
                        Log.d("ChooseCity/VIEW WEATHER", "Celsius selected.");
                        metric = "metric";
                    }
                    else
                    {
                        metric = "imperial";
                        Log.d("ChooseCity/VIEW WEATHER", "Fahrenheit selected.");
                    }

                    Intent intent = new Intent(ChooseCity.this, MainActivity.class);
                    intent.putExtra("id", city.CityId()); // Set your ID as a Intent Extra
                    intent.putExtra("name", city.CityName()); // Set your ID as a Intent Extra
                    intent.putExtra("lat", city.Lat()); // Set your ID as a Intent Extra
                    intent.putExtra("lng", city.Lng()); // Set your ID as a Intent Extra
                    intent.putExtra("metric", metric);

                    Log.d("ChooseCity/VIEW WEATHER", "CITY ID: " + city.CityId());

                    startActivity(intent);
                }
                else if (qtyCheckd == 0) {
                    Toast.makeText(ChooseCity.this, "Choose one city!", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(ChooseCity.this, "Choose only one city", Toast.LENGTH_LONG).show();
            }
        });

        // *** SEARCH CITY BUTTON *** //
        _btnSearchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                URL url = null;
                l2.removeAllViews();
                rbg.removeAllViews();

                cityList = new ArrayList<>();

                if (_txtNewLocation.getText().toString().matches(""))
                    Toast.makeText(ChooseCity.this, "Insert a city", Toast.LENGTH_LONG).show();
                else {
                    try {
                        Log.d("ChooseCity/SEARCH CITY", URL_CITY_LIST_API.toString());
                        url = new URL(String.format(URL_CITY_LIST_API, _txtNewLocation.getText()));

                        HttpURLConnection httpConn = null;
                        httpConn = (HttpURLConnection) url.openConnection();

                        Log.d("ChooseCity/CITY-CONN", url.toString());
                        BufferedReader bufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));

                        StringBuffer json = new StringBuffer(1024);
                        String singleLine = "";

                        while ((singleLine = bufReader.readLine()) != null)
                            json.append(singleLine).append("\n");

                        Log.d("ChooseCity/CITY-LIST", json + "");
                        bufReader.close();

                        JSONObject jObject = new JSONObject(json.toString());
                        int cityCount = Integer.parseInt(jObject.getJSONObject("metadata").getString("totalCount"));

                        Log.d("M ACT/GETJSONWEATHER", "cityCount: " + cityCount);

                        if (cityCount > 0) {
                            cityCount = (cityCount > 5 ? 5 : cityCount);

                            for (int i = 0; i <= cityCount - 1; i++) {
                                JSONObject jsonORoot = (JSONObject) jObject.getJSONArray("data").get(i);
                                //Log.d("M ACT/GETJSONWEATHER", "" + jsonORoot);

                                int cityId = jsonORoot.getInt("id");
                                String cityName = jsonORoot.getString("city");
                                Double cityLat = jsonORoot.getDouble("latitude");
                                Double cityLng = jsonORoot.getDouble("longitude");
                                String countryCode = jsonORoot.getString("country");

                                City city = new City(cityId, cityName, cityLat, cityLng, countryCode);
                                cityList.add(city);

                                Log.d("ChooseCity/CITY-ITEM", cityId + " / " + cityName + " / " + cityLat + " / " + cityLng);
                            }

                            for (int i = 0; i < cityList.size(); i++) {
                                rb = new RadioButton(ChooseCity.this);
                                rb.setId(cityList.get(i).CityId());
                                rb.setText(cityList.get(i).CityName() + ", " + cityList.get(i).CountryCode());
                                rbg.addView(rb);
                            }

                            l2.addView(rbg);
                        } else
                            Toast.makeText(ChooseCity.this, "City not found", Toast.LENGTH_LONG).show();
                    } catch (IOException | JSONException e) {
                        Log.d("ChooseCity/CITY-ERR", "ERROR: " + e.getMessage());
                    }
                }
            }
        });

        // *** BACK MAIN CITY BUTTON *** //
        btnBackMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void AddLocation() {
        _btnAddNewCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                City city = null;
                boolean cityExists = false;

                for (int i = 0; i < rbg.getChildCount(); i++) {
                    RadioButton radioSelectedCity = (RadioButton) rbg.getChildAt(i);

                    if (radioSelectedCity.isChecked()) {
                        Log.d("ChooseCity/AddLocation", "SELECTED CITY ID -> " + String.valueOf(radioSelectedCity.getId()));
                        city = GetCityInArray(radioSelectedCity.getId(), cityList);
                    }
                }

                for (int i = 0; i < SQLLocations.size(); i++) {
                    if (city.CityId() == SQLLocations.get(i).CityId())
                    {
                        cityExists = true;
                        break;
                    }
                }

                boolean isInserted = false;

                if (!cityExists)
                {
                    if (city != null) {
                        Log.d("ChooseCity/AddLocation", "SELECTED CITY -> " + String.valueOf(city.CityName()));
                        isInserted = myDB.insertData(city.CityId(), city.CityName(), city.Lat(), city.Lng(), city.CountryCode());
                    }
                    else {
                        Log.d("ChooseCity/AddLocation", "SELECTED CITY -> NOT FOUND");
                    }

                    Log.d("CHOOSE CITY/AddLocation", "isInserted: " + isInserted);

                    if (isInserted == true){
                        Toast.makeText(ChooseCity.this, "City added!", Toast.LENGTH_LONG).show();
                        FillLocationsRecord();
                    }
                    else
                        Toast.makeText(ChooseCity.this, "Select the city!", Toast.LENGTH_LONG).show();
                }
                else
                    Toast.makeText(ChooseCity.this, "City already on the list!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void FillLocationsRecord() {
        //Get Locations already in the database
        LinearLayout ll3 = findViewById(R.id.Lresult);
        ll3.removeAllViews();

        SQLLocations = GetSQLLocationsSQL();

        CheckBox cb = new CheckBox(this);
        cb.setId(0);
        cb.setText("[GPS Location]");
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _gpsLocationCityId = 0;
            }
        });

        ll3.addView(cb);

        //Feed Second ListView
        for (int i = 0 ; i < SQLLocations.size(); i++) {//Add the four in four because I want to show only the City name!!!!!
            cb = new CheckBox(this);
            cb.setId(SQLLocations.get(i).CityId());
            cb.setText(SQLLocations.get(i).CityName() + ", " + SQLLocations.get(i).CountryCode());

            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _gpsLocationCityId = -1;
                }
            });

            ll3.addView(cb);
        }
    }

    private ArrayList<String> GetPossibleLocationsAPI(String NewLocation ){
        //Get Locations from API returns an ArrayList
        ArrayList<String> CityList = new ArrayList<String>();
        Toast.makeText(ChooseCity.this, "Lista criada", Toast.LENGTH_LONG).show();
        CityList.add(NewLocation+"1");
        CityList.add(NewLocation+"2");
        CityList.add(NewLocation+"3");

        return CityList;
    }

    private ArrayList<City> GetSQLLocationsSQL() {
        //All Locations From SQLite
        Cursor res = myDB.getAllData();
        int DBsize = res.getCount();

        ArrayList<City> CityList = new ArrayList<City>();
        Log.d("CHOOSE CITY/GetSQLLoc", "DBSize " + DBsize);

        String City = "";
        int count = 1;

        while(res.moveToNext()){
            CityList.add(new City(res.getInt(0), (String)res.getString(1), res.getDouble(2), res.getDouble(3), res.getString(4)));
            Log.d("CHOOSE CITY/GetSQLLoc", "i(" + count + "): " + res.getInt(0) + " / " + res.getString(1));

            count++;
        }

        return CityList;
    }

    private City GetCityInArray(int id, ArrayList<City> cityListToSearch)
    {
        City city = null;
        for (int i = 0; i < cityListToSearch.size(); i++) {
            Log.d("CHOOSE CITY/GetCityArr", "CityId: " + cityListToSearch.get(i).CityId());

            if (cityListToSearch.get(i).CityId() == id) {
                city = cityListToSearch.get(i);
                break;
            }
        }

        return city;
    }
}