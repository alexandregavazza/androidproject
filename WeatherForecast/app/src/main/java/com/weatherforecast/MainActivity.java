package com.weatherforecast;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static android.location.LocationManager.GPS_PROVIDER;

public class MainActivity extends AppCompatActivity {
    private TextView txtLocation;
    private boolean getFromChooseCity = false;
    public double latitude = 0;
    public double longitude = 0;
    public String locality = "";
    public String country = "";
    public String metric = "";

    private boolean getWeatherFromGPS = true;

    private double temperature = 0.0;
    private double minTemp = 0.0;
    private double maxTemp = 0.0;
    private String weatherDescription = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("M ACT/ONCREATE", "CALLING...");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        txtLocation = (TextView) findViewById(R.id.txtLocation);

        //Add new cities
        ImageView imgFindCity = (ImageView) findViewById(R.id.imgFindCity);
        imgFindCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ChooseCity.class));
            }
        });

        // GET CURRENT WEATHER
        Intent intentInfo = getIntent();
        Log.d("M ACT/ONCREATE", "Extra intent info step.");

        if (intentInfo != null && intentInfo.getExtras() != null) {
            Log.d("M ACT/ONCREATE", "RETRIEVING DATA FROM CHOOSECITY...");
            int cityId = (int) intentInfo.getIntExtra("id", 0);
            locality = intentInfo.getStringExtra("name");
            latitude = (Double) intentInfo.getDoubleExtra("lat", 0.0);
            longitude = (Double) intentInfo.getDoubleExtra("lng", 0.0);
            metric = intentInfo.getStringExtra("metric");

            Log.d("M ACT/ONCREATE", "RETRIEVED: " + cityId + " / " + locality + " / " + latitude + " / " + longitude);

            if (cityId == 0)
                getFromChooseCity = false;
            else
                getFromChooseCity = true;
        }

        if (!getFromChooseCity) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            LocationListener GPSLocationListener = new GPSLocationListener();

            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.d("M ACT/ONCREATE", "GPS ACCESS DISABLED.");

                    // FORCE TO GO TO CELLPHONE SETTINGS TO CHANGE GPS PERMISSION
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                } else
                    Log.d("M ACT/ONCREATE", "GPS ACCESS ENABLED.");

                locationManager.requestLocationUpdates(GPS_PROVIDER, 5000, 10, GPSLocationListener);
            } catch (Exception ex) {
                Log.d("M ACT/ONCREATE/ONCREATE", "ERROR: " + ex.getMessage());
            } finally {
                Log.d("M ACT/ONCREATE/ONCREATE", "LOCATION IS DONE.");
            }
        }
        else
            UpdateLocation(null, getFromChooseCity);

        Log.d("M ACT/ONCREATE", "TERMINATED...");
    }

    public class GPSLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            UpdateLocation(location, false);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.d("M ACT/ONLOCATIONCHANGE", "onStatusChanged...");
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.d("M ACT/ONLOCATIONCHANGE", "onProviderEnabled...");
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.d("M ACT/ONLOCATIONCHANGE", "onProviderDisabled...");
        }
    }

    public class OpenWeatherAPI {
        private static final String URL_OPEN_WEATHER_MAP_API = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=%s";

        public JSONObject getJSONWeather(Context context, String locality) {
            try {
                Log.d("M ACT/GETJSONWEATHER", "CALLING [" + locality + "]...");
                URL url = new URL(String.format(URL_OPEN_WEATHER_MAP_API, locality, metric));
                HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();

                Log.d("M ACT/GETJSONWEATHER", url.toString());
                httpConn.addRequestProperty("x-api-key", "04938ac2bc4b113ab61c91062e68a3d9");
                BufferedReader bufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));

                StringBuffer json = new StringBuffer(1024);
                String singleLine = "";

                while ((singleLine = bufReader.readLine()) != null)
                    json.append(singleLine).append("\n");

                Log.d("M ACT/GETJSONWEATHER", "JSON: " + json);
                bufReader.close();
                JSONObject jObject = new JSONObject(json.toString());

                // 404 for not successful
                if (jObject.getInt("cod") != 200)
                    Log.d("M ACT/GETJSONWEATHER", "Error on getting OPEN WEATHER API.\n");

                return jObject;
            } catch (IOException | JSONException e) {
                Log.d("M ACT/GETJSONWEATHER", e.getMessage());
            }

            Log.d("M ACT/GETJSONWEATHER", "TERMINATED...");
            return null;
        }
    }

    public void UpdateLocation(Location location, boolean getFromChooseCity)
    {
        Log.d("M ACT/ONLOCATIONCHANGE", "CALLING...");

        if (!getFromChooseCity) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

        // LABELS
        TextView txtLabelTemp = (TextView) findViewById(R.id.txtActualTemp);
        TextView txtLabelTempMin = (TextView) findViewById(R.id.txtMin);
        TextView txtLabelTempMax = (TextView) findViewById(R.id.txtMax);
        TextView txtLabelWeather = (TextView) findViewById(R.id.txtWeather);

        boolean validLocation = false;
        locality = "[NO LOCATION]";
        country  = "[NO LOCATION]";

        Log.d("M ACT/ONLOCATIONCHANGE", "LAT / LNG: " + latitude + " / " + longitude);
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocation(latitude, longitude, 1);

            Log.d("M ACT/ONLOCATIONCHANGE", "addressList.size(): " + addressList.size());

            if (addressList.size() > 0) {
                Log.d("M ACT/ONLOCATIONCHANGE", addressList.toString());

                if (addressList.get(0).getSubAdminArea() != null || addressList.get(0).getLocality() != null) {
                    if (addressList.get(0).getLocality() != null) {
                        locality = addressList.get(0).getLocality();
                        country  = addressList.get(0).getCountryName();
                        Log.d("M ACT/ONLOCATIONCHANGE", "SUB-ADMIN_AREA: " + locality);
                        validLocation = true;
                    } else if (addressList.get(0).getSubAdminArea() != null) {
                        locality = addressList.get(0).getSubAdminArea();
                        country  = addressList.get(0).getCountryName();
                        Log.d("M ACT/ONLOCATIONCHANGE", "LOCALITY: " + locality);
                        validLocation = true;
                    }
                }
            }

            Log.d("M ACT/ONLOCATIONCHANGE", "VALIDLOCATION: " + validLocation);

            if (locality == null) {
                validLocation = false;
                locality = "[NO LOCATION]";
            }

            if (validLocation) {
                OpenWeatherAPI open = new OpenWeatherAPI();

                // Some cities come with 'City' in the locality property and the OpenWeatherAPI do not find it
                // To fix this problem we removed the 'city'
                if (locality.indexOf("City") != -1)
                    locality = locality.replace(" City", "");

                locality = locality.replace("'", "");
                Log.d("M ACT/ONLOCATIONCHANGE", "Final location: " + locality);

                JSONObject jsonO = open.getJSONWeather(getBaseContext(), locality);

                if (jsonO == null)
                    locality = "[LOCATION NOT FOUND]";
                else {
                    Log.d("M ACT/ONLOCATIONCHANGE", "jsonO: " + jsonO.toString());

                    JSONObject jsonOMain = jsonO.getJSONObject("main");

                    JSONArray jsonOWeatherArray = jsonO.getJSONArray("weather");
                    JSONObject jsonOWeather = jsonOWeatherArray.getJSONObject(0);

                    temperature = jsonOMain.getDouble("temp");
                    minTemp = jsonOMain.getDouble("temp_min");
                    maxTemp = jsonOMain.getDouble("temp_max");
                    weatherDescription = jsonOWeather.getString("main");
                    String weatherIcon = jsonOWeather.getString("icon");
                    String weatherDetail = jsonOWeather.getString("description");
                    String iconURL = "http://openweathermap.org/img/wn/" + weatherIcon + "@2x.png";

                    //ICON
                    ImageView imgIcon = findViewById(R.id.imgIcon);
                    Picasso.get().load(iconURL).into(imgIcon);
                }
            }

            Log.d("GPS LISTENER", temperature + " / " + minTemp + " / " + maxTemp);
        } catch (IOException e) {
            Log.d("GPS LISTENER", e.getMessage());
        } catch (JSONException e) {
            Log.d("GPS LISTENER", e.getMessage());
        } finally {
            txtLocation.setText(locality);

            String initial = (metric.equals("metric") ? "°C" : "°F");
            Log.d("GPS LISTENER", "initial: " + initial);

            txtLabelTemp.setText(String.format("%.0f", temperature) + initial);
            txtLabelTempMin.setText(String.format("%.0f", minTemp) + initial);
            txtLabelTempMax.setText(String.format("%.0f", maxTemp) + initial);
            txtLabelWeather.setText(weatherDescription);

            String unplashURL = "https://source.unsplash.com/600x800/?" + locality + "," + country + ",city";

            Picasso.get()
                    .load(unplashURL)
                    .into(new Target() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            ConstraintLayout consLayout = (ConstraintLayout) findViewById(R.id.weatherLayout);
                            consLayout.setBackground(new BitmapDrawable(bitmap));
                            Log.d("M ACT/ONLOCATIONCHANGE", "Image loaded!");
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            Log.d("M ACT/ONLOCATIONCHANGE", "onBitmapFailed!");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            Log.d("M ACT/ONLOCATIONCHANGE", "onPrepareLoad!");
                        }
                    });
            Log.d("M ACT/ONLOCATIONCHANGE", unplashURL);
        }

        Log.d("M ACT/ONLOCATIONCHANGE", "TERMINATED...");
    }
}